<?php

function mm_features_banish_features_banish_alter(&$banished) {
  global $conf;
  $banished += array('variable' => array());
  
  $patterns = array(
    '__active_tab$',
    '_lastrun$',
    '_rebuild_needed$',
    '^uc-progress:',
    '^cache_flush_',
    '^backup_migrate_schedule_last_run_',
    'cron_last$',
    'last_cron$',
    '^uc-progress:',
    '^color_.*_files',
    '^color_.*_logo',
    '^color_.*_stylesheets',
    '^cache_content_flush_cache',
    '^cache_temporary_flush_cache',
    '^additional_settings__active_tab_',
  );
  foreach ($conf as $key => $_) {
    foreach ($patterns as $pattern) {
      if (preg_match("~$pattern~", $key)) {
        $banished['variable'][] = $key;
      }
    }
  }
}

